const express = require('express')
const fileUpload = require('express-fileupload')
const app = express()
const port = 3000
var bodyParser = require('body-parser')
app.use(bodyParser.json());

// parse various different custom JSON types as JSON
//app.use(bodyParser.json({ type: 'application/*+json' }))
app.use(fileUpload())

app.post('/api/upload', (req, res) => {
    if (req.files.file == undefined) {
        return res.status(400).json('File is required');
    }

    if (req.body.projectName == undefined || req.body.projectName == "") {
        return res.status(400).json('Project name is required');
    }

    const file = req.files.file
    const dataString = file.data.toString('utf-8')
    let dataJson;
    try {
        dataJson = JSON.parse(dataString);
    } catch (e) {
        console.log("Invalid JSON");
        return res.status(400).json('Invalid JSON');
    }
    const projectName = req.body.projectName

    for (const key in dataJson) {
        if (dataJson.hasOwnProperty(key)) {
            const value = dataJson[key];
            console.log(`La key ${key} tiene el valor ${value}`);
            insertKeyInDB(key, value, projectName)
        }
    }

    return res.json({ username: 'Flavio' });
})

async function insertKeyInDB(key, value, projectName) {

    //----------
    const { Pool, Client } = require('pg')
    // pools will use environment variables
    // for connection information
    const pool = new Pool()
    const queryStr =`insert into translations ("key", "value", "project_name", "language") values ('${key}', '${value}', '${projectName}', 'es' ), ('${key}', ' ', '${projectName}', 'en' )`
    console.log('queryStr', queryStr);
    pool.query(queryStr, (err, res) => {
        console.log(err, res)
        pool.end()
    })
    // you can also use async/await
    /* const res = await pool.query('SELECT NOW()')
    await pool.end()
    // clients will also use environment variables
    // for connection information
    const client = new Client()
    await client.connect()
    const res = await client.query('SELECT NOW()')
    await client.end() */
    //----------------


    console.log(`Inserting ${key} and ${value} for ${projectName}`);
}

app.get('/api/gettranslations', (req, response) => {
    //----------
    const { Pool, Client } = require('pg')
    // pools will use environment variables
    // for connection information
    const pool = new Pool()
    const queryStr = 'select * from translations'
    console.log('queryStr', queryStr);
    pool.query(queryStr, (err, result) => {
        console.log(err, result)
        pool.end()
        return response.json(result.rows)
    })
})

app.put('/api/update', async (req, res) => {
    console.log('req', req.body);
    //----------
    const { Pool, Client } = require('pg')
    // pools will use environment variables
    // for connection information
    const pool = new Pool()
    for (let i = 0; i < req.body.length; i++) {
        const element = req.body[i];
        
        const queryStr =`update translations set "value" = '${element.value}' where id = ${element.id}`
        console.log('queryStr', queryStr);
        await pool.query(queryStr, (err, res) => {
            console.log(err, res)
            pool.end()
        })
    }
    return res.json('ok')
})

app.get('/api/', (req, res) => {
    res.send('Hello World!')
})

app.listen(port, () => {
    console.log(`Example app listening at http://localhost:${port}`)
})

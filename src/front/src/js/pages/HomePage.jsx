import React, {useEffect, useState} from 'react'
import './HomePage.scss'
import Input from '../components/Input/Input'
import Header from '../components/Header/Header'
import TranslateItem from '../components/TranslateItem/TranslateItem'
import { jsonToArray } from '../hooks/jsonToArray'

const HomePage = () => {
    const [array, setArray] = useState([]);

    useEffect(()=>{
        const jsonData = { HELLO : 'Hola', BYE : 'Adios'};
        setArray(jsonToArray(jsonData));
        console.log('array', array);    
    }, [])

    const handleInputChange = (value) => {
        console.log('input handle desde home', value);
    }

    const handleInputBlur = (value) => {
        console.log('input blur handle desde home', value);
    }

    const handleInputFileChange = () => {
        alert('hhhh')
    }

    const handleSaveClick= () => {
        console.log('save');
    }

    return (
        <div className="home-page-container">
            {/* <Input
                onChange={handleInputChange}
                onBlur={handleInputBlur}
                placeHolder="escribe una cosa"
            /> */}
            <div className="header"><Header onFileChange={handleInputFileChange}/></div>
            <div className="home-page-content grid-x">
                <div className="side-bar cell medium-4 large-4">side bar</div>
                <div className="content cell medium-8 large-8">
                    <div className="btn-save button" onClick={handleSaveClick}>Save</div>
                    {
                        array.map((translateItem, index) =>{
                            return (
                                <TranslateItem 
                                    key={index} 
                                    value={translateItem} 
                                    index={index + 1}
                                />
                            )
                        })
                    }
                </div>
            </div>
        </div>
    )
}

export default HomePage
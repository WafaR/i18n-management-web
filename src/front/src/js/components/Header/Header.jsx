import React from 'react';

const Header = (props) => {

    const handleFileChange = () =>{
        props.onFileChange()
    }

    return (
        <div className="header-container">
            <input onChange={handleFileChange} type="file"/>
        </div>
    )
}

export default Header
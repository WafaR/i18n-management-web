import React from 'react';
import './BulletNumber.scss';

const BulletNumber = ({number, size=20}) => {
//const BulletNumber = (props) => {
  //  const {number} = props

    return (
        <div className="bullet-number-container" style={{height: `${size}px`, width: `${size}px`}}>{ number }</div>
    )

}

export default BulletNumber